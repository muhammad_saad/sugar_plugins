<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_TAGS_LINK' => 'Etiketler',
  'LBL_TAGS' => 'Etiketler',
  'LBL_ID' => 'KİMLİK',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_DOC_OWNER' => 'Doküman Sahibi',
  'LBL_USER_FAVORITES' => 'Favori Olan Kullanıcılar',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_DELETED' => 'Silindi',
  'LBL_NAME' => 'İsim',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_REMOVE' => 'Sil',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_LIST_FORM_TITLE' => 'MC Configurations Liste',
  'LBL_MODULE_NAME' => 'MC Configurations',
  'LBL_MODULE_TITLE' => 'MC Configurations',
  'LBL_MODULE_NAME_SINGULAR' => 'MC Configuration',
  'LBL_HOMEPAGE_TITLE' => 'Benim MC Configurations',
  'LNK_NEW_RECORD' => 'Oluştur MC Configuration',
  'LNK_LIST' => 'Göster MC Configurations',
  'LNK_IMPORT_Z_MCCONFIG' => 'Import MC Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Ara MC Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_Z_MCCONFIG_SUBPANEL_TITLE' => 'MC Configurations',
  'LBL_NEW_FORM_TITLE' => 'Yeni MC Configuration',
  'LNK_IMPORT_VCARD' => 'Import MC Configuration vCard',
  'LBL_IMPORT' => 'Import MC Configurations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new MC Configuration record by importing a vCard from your file system.',
  'LBL_MC_LIST_ID' => 'MC List Id',
  'LBL_PROSPECT_LIST_ID' => 'Prospect List Id',
  'LBL_TARGET_MODULE_ID' => 'Target Module Id',
  'LBL_IS_SYNCED' => 'Is Synced?',
  'LBL_SYNC_STATUS' => 'Sync Status',
  'LBL_MC_MEMBER_ID' => 'MC Member Id',
);