<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Gruppi',
  'LBL_TEAMS' => 'Gruppi',
  'LBL_TEAM_ID' => 'Id Gruppo',
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_TAGS_LINK' => 'Tag',
  'LBL_TAGS' => 'Tag',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DOC_OWNER' => 'Proprietario documento',
  'LBL_USER_FAVORITES' => 'Preferenze Utenti',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Cancellato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Modifica',
  'LBL_REMOVE' => 'Rimuovi',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificato dal Nome',
  'LBL_LIST_FORM_TITLE' => 'MC Configurations Lista',
  'LBL_MODULE_NAME' => 'MC Configurations',
  'LBL_MODULE_TITLE' => 'MC Configurations',
  'LBL_MODULE_NAME_SINGULAR' => 'MC Configuration',
  'LBL_HOMEPAGE_TITLE' => 'Mio MC Configurations',
  'LNK_NEW_RECORD' => 'Crea MC Configuration',
  'LNK_LIST' => 'Visualizza MC Configurations',
  'LNK_IMPORT_Z_MCCONFIG' => 'Import MC Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca MC Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_Z_MCCONFIG_SUBPANEL_TITLE' => 'MC Configurations',
  'LBL_NEW_FORM_TITLE' => 'Nuovo MC Configuration',
  'LNK_IMPORT_VCARD' => 'Import MC Configuration vCard',
  'LBL_IMPORT' => 'Import MC Configurations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new MC Configuration record by importing a vCard from your file system.',
  'LBL_MC_LIST_ID' => 'MC List Id',
  'LBL_PROSPECT_LIST_ID' => 'Prospect List Id',
  'LBL_TARGET_MODULE_ID' => 'Target Module Id',
  'LBL_IS_SYNCED' => 'Is Synced?',
  'LBL_SYNC_STATUS' => 'Sync Status',
  'LBL_MC_MEMBER_ID' => 'MC Member Id',
);