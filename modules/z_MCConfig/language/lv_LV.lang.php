<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotāja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_TAGS_LINK' => 'Birkas',
  'LBL_TAGS' => 'Birkas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveides datums',
  'LBL_DATE_MODIFIED' => 'Modificēšanas datums',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēts pēc vārda',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificēts pēc vārda',
  'LBL_LIST_FORM_TITLE' => 'MC Configurations Saraksts',
  'LBL_MODULE_NAME' => 'MC Configurations',
  'LBL_MODULE_TITLE' => 'MC Configurations',
  'LBL_MODULE_NAME_SINGULAR' => 'MC Configuration',
  'LBL_HOMEPAGE_TITLE' => 'Mans MC Configurations',
  'LNK_NEW_RECORD' => 'Izveidot MC Configuration',
  'LNK_LIST' => 'Skats MC Configurations',
  'LNK_IMPORT_Z_MCCONFIG' => 'Import MC Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt MC Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_Z_MCCONFIG_SUBPANEL_TITLE' => 'MC Configurations',
  'LBL_NEW_FORM_TITLE' => 'Jauns MC Configuration',
  'LNK_IMPORT_VCARD' => 'Import MC Configuration vCard',
  'LBL_IMPORT' => 'Import MC Configurations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new MC Configuration record by importing a vCard from your file system.',
  'LBL_MC_LIST_ID' => 'MC List Id',
  'LBL_PROSPECT_LIST_ID' => 'Prospect List Id',
  'LBL_TARGET_MODULE_ID' => 'Target Module Id',
  'LBL_IS_SYNCED' => 'Is Synced?',
  'LBL_SYNC_STATUS' => 'Sync Status',
  'LBL_MC_MEMBER_ID' => 'MC Member Id',
);