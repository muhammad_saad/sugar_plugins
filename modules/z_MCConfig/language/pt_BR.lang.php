<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Equipes',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Id da Equipe',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Marcações',
  'LBL_TAGS' => 'Marcações',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado por nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DOC_OWNER' => 'Proprietário do documento',
  'LBL_USER_FAVORITES' => 'Usuários Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado por nome',
  'LBL_LIST_FORM_TITLE' => 'MC Configurations Lista',
  'LBL_MODULE_NAME' => 'MC Configurations',
  'LBL_MODULE_TITLE' => 'MC Configurations',
  'LBL_MODULE_NAME_SINGULAR' => 'MC Configuration',
  'LBL_HOMEPAGE_TITLE' => 'Minha MC Configurations',
  'LNK_NEW_RECORD' => 'Criar MC Configuration',
  'LNK_LIST' => 'Visualização MC Configurations',
  'LNK_IMPORT_Z_MCCONFIG' => 'Import MC Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar MC Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Cadeia de atividades',
  'LBL_Z_MCCONFIG_SUBPANEL_TITLE' => 'MC Configurations',
  'LBL_NEW_FORM_TITLE' => 'Novo MC Configuration',
  'LNK_IMPORT_VCARD' => 'Import MC Configuration vCard',
  'LBL_IMPORT' => 'Import MC Configurations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new MC Configuration record by importing a vCard from your file system.',
  'LBL_MC_LIST_ID' => 'MC List Id',
  'LBL_PROSPECT_LIST_ID' => 'Prospect List Id',
  'LBL_TARGET_MODULE_ID' => 'Target Module Id',
  'LBL_IS_SYNCED' => 'Is Synced?',
  'LBL_SYNC_STATUS' => 'Sync Status',
  'LBL_MC_MEMBER_ID' => 'MC Member Id',
);