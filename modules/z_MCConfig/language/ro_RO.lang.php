<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Id Echipă',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuit lui',
  'LBL_TAGS_LINK' => 'Etichete',
  'LBL_TAGS' => 'Etichete',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data creării',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificat după Id',
  'LBL_MODIFIED_NAME' => 'Modificat după Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creat de Id',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Şters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creat de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificat după Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Eliminare',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_LIST_FORM_TITLE' => 'MC Configurations Lista',
  'LBL_MODULE_NAME' => 'MC Configurations',
  'LBL_MODULE_TITLE' => 'MC Configurations',
  'LBL_MODULE_NAME_SINGULAR' => 'MC Configuration',
  'LBL_HOMEPAGE_TITLE' => 'Al meu MC Configurations',
  'LNK_NEW_RECORD' => 'Creează MC Configuration',
  'LNK_LIST' => 'Vizualizare MC Configurations',
  'LNK_IMPORT_Z_MCCONFIG' => 'Import MC Configuration',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta MC Configuration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_Z_MCCONFIG_SUBPANEL_TITLE' => 'MC Configurations',
  'LBL_NEW_FORM_TITLE' => 'Nou MC Configuration',
  'LNK_IMPORT_VCARD' => 'Import MC Configuration vCard',
  'LBL_IMPORT' => 'Import MC Configurations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new MC Configuration record by importing a vCard from your file system.',
  'LBL_MC_LIST_ID' => 'MC List Id',
  'LBL_PROSPECT_LIST_ID' => 'Prospect List Id',
  'LBL_TARGET_MODULE_ID' => 'Target Module Id',
  'LBL_IS_SYNCED' => 'Is Synced?',
  'LBL_SYNC_STATUS' => 'Sync Status',
  'LBL_MC_MEMBER_ID' => 'MC Member Id',
);