<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$dictionary['z_MCConfig'] = array(
	'table' => 'z_mcconfig',
	'audited' => false,
	'activity_enabled' => false,
	'duplicate_merge' => false,
	'fields' => array(
		'mc_list_id' =>
		array(
			'required' => false,
			'name' => 'mc_list_id',
			'vname' => 'LBL_MC_LIST_ID',
			'type' => 'varchar',
			'massupdate' => false,
			'default' => '',
			'no_default' => false,
			'comments' => '',
			'help' => '',
			'importable' => 'true',
			'duplicate_merge' => 'enabled',
			'duplicate_merge_dom_value' => '1',
			'audited' => false,
			'reportable' => true,
			'unified_search' => false,
			'merge_filter' => 'disabled',
			'full_text_search' =>
			array(
				'enabled' => '0',
				'boost' => '1',
				'searchable' => false,
			),
			'calculated' => false,
			'len' => '50',
			'size' => '20',
		),
		'prospect_list_id' =>
		array(
			'required' => false,
			'name' => 'prospect_list_id',
			'vname' => 'LBL_PROSPECT_LIST_ID',
			'type' => 'varchar',
			'massupdate' => false,
			'default' => '',
			'no_default' => false,
			'comments' => '',
			'help' => '',
			'importable' => 'true',
			'duplicate_merge' => 'enabled',
			'duplicate_merge_dom_value' => '1',
			'audited' => false,
			'reportable' => true,
			'unified_search' => false,
			'merge_filter' => 'disabled',
			'full_text_search' =>
			array(
				'enabled' => '0',
				'boost' => '1',
				'searchable' => false,
			),
			'calculated' => false,
			'len' => '36',
			'size' => '20',
		),
		'target_module_id' =>
		array(
			'required' => false,
			'name' => 'target_module_id',
			'vname' => 'LBL_TARGET_MODULE_ID',
			'type' => 'varchar',
			'massupdate' => false,
			'default' => '',
			'no_default' => false,
			'comments' => '',
			'help' => '',
			'importable' => 'true',
			'duplicate_merge' => 'enabled',
			'duplicate_merge_dom_value' => '1',
			'audited' => false,
			'reportable' => true,
			'unified_search' => false,
			'merge_filter' => 'disabled',
			'full_text_search' =>
			array(
				'enabled' => '0',
				'boost' => '1',
				'searchable' => false,
			),
			'calculated' => false,
			'len' => '36',
			'size' => '20',
		),
		'target_module' =>
		array(
			'required' => false,
			'name' => 'target_module',
			'vname' => 'LBL_TARGET_MODULE',
			'type' => 'varchar',
			'massupdate' => false,
			'default' => '',
			'no_default' => false,
			'comments' => '',
			'help' => '',
			'importable' => 'true',
			'duplicate_merge' => 'enabled',
			'duplicate_merge_dom_value' => '1',
			'audited' => false,
			'reportable' => true,
			'unified_search' => false,
			'merge_filter' => 'disabled',
			'full_text_search' =>
			array(
				'enabled' => '0',
				'boost' => '1',
				'searchable' => false,
			),
			'calculated' => false,
			'len' => '50',
			'size' => '20',
		),
		'is_synced' =>
		array(
			'required' => false,
			'name' => 'is_synced',
			'vname' => 'LBL_IS_SYNCED',
			'type' => 'bool',
			'massupdate' => false,
			'default' => false,
			'no_default' => false,
			'comments' => '',
			'help' => '',
			'importable' => 'true',
			'duplicate_merge' => 'enabled',
			'duplicate_merge_dom_value' => '1',
			'audited' => false,
			'reportable' => true,
			'unified_search' => false,
			'merge_filter' => 'disabled',
			'calculated' => false,
			'len' => '255',
			'size' => '20',
		),
		'sync_status' =>
		array(
			'required' => false,
			'name' => 'sync_status',
			'vname' => 'LBL_SYNC_STATUS',
			'type' => 'enum',
			'massupdate' => true,
			'default' => 'Active',
			'no_default' => false,
			'comments' => '',
			'help' => '',
			'importable' => 'true',
			'duplicate_merge' => 'enabled',
			'duplicate_merge_dom_value' => '1',
			'audited' => false,
			'reportable' => true,
			'unified_search' => false,
			'merge_filter' => 'disabled',
			'calculated' => false,
			'len' => 100,
			'size' => '20',
			'options' => 'sync_status_list',
			'dependency' => false,
		),
		'mc_member_id' =>
		array(
			'required' => false,
			'name' => 'mc_member_id',
			'vname' => 'LBL_MC_MEMBER_ID',
			'type' => 'varchar',
			'massupdate' => false,
			'default' => '',
			'no_default' => false,
			'comments' => '',
			'help' => '',
			'importable' => 'true',
			'duplicate_merge' => 'enabled',
			'duplicate_merge_dom_value' => '1',
			'audited' => false,
			'reportable' => true,
			'unified_search' => false,
			'merge_filter' => 'disabled',
			'full_text_search' =>
			array(
				'enabled' => '0',
				'boost' => '1',
				'searchable' => false,
			),
			'calculated' => false,
			'len' => '255',
			'size' => '20',
		),
		'email_address' =>
		array(
			'required' => false,
			'name' => 'email_address',
			'vname' => 'LBL_EMAIL_ADDRESS',
			'type' => 'varchar',
			'massupdate' => false,
			'default' => '',
			'no_default' => false,
			'comments' => '',
			'help' => '',
			'importable' => 'true',
			'duplicate_merge' => 'enabled',
			'duplicate_merge_dom_value' => '1',
			'audited' => false,
			'reportable' => true,
			'unified_search' => false,
			'merge_filter' => 'disabled',
			'full_text_search' =>
			array(
				'enabled' => '0',
				'boost' => '1',
				'searchable' => false,
			),
			'calculated' => false,
			'len' => '255',
			'size' => '20',
		),
	),
	'relationships' => array(
	),
	'optimistic_locking' => true,
	'unified_search' => true,
	'full_text_search' => true,
);

if (!class_exists('VardefManager')) {
	require_once 'include/SugarObjects/VardefManager.php';
}
VardefManager::createVardef('z_MCConfig', 'z_MCConfig', array('basic', 'team_security', 'assignable', 'taggable'));
