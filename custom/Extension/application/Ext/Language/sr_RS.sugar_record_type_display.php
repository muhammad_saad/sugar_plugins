<?php
 // created: 2017-04-17 05:08:01

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Kompanija',
  'Opportunities' => 'Prodajna prilika',
  'Cases' => 'Slučaj:',
  'Leads' => 'Potencijalni klijent',
  'Contacts' => 'Kontakti',
  'Products' => 'Proizvod',
  'Quotes' => 'Ponuda',
  'Bugs' => 'Defekt:',
  'Project' => 'Projekat',
  'Prospects' => 'Cilj',
  'ProjectTask' => 'Projektni Zadatak',
  'Tasks' => 'Zadatak',
  'KBContents' => 'Baza Znanja',
  'RevenueLineItems' => 'Stavke prihoda',
);