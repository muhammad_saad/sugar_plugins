<?php
 // created: 2017-04-17 05:07:51

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Konto',
  'Opportunities' => 'Affärsmöjlighet',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Contacts' => 'Kontakter',
  'Products' => 'Produkt',
  'Quotes' => 'Offert',
  'Bugs' => 'Bugg',
  'Project' => 'Projekt',
  'Prospects' => 'Mål',
  'ProjectTask' => 'Projektuppgift',
  'Tasks' => 'Uppgift',
  'KBContents' => 'Kunskapsbas Dokument',
  'RevenueLineItems' => 'Intäktsposter',
);