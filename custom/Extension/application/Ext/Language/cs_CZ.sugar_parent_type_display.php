<?php
 // created: 2017-04-17 05:07:18

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Společnost',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Úkol',
  'Opportunities' => 'Příležitost',
  'Products' => 'Produkt',
  'Quotes' => 'Nabídka',
  'Bugs' => 'Chyby',
  'Cases' => 'Případ:',
  'Leads' => 'Příležitost',
  'Project' => 'Projekty',
  'ProjectTask' => 'Projektové úkoly',
  'Prospects' => 'Kontakt',
  'KBContents' => 'Znalostní báze',
  'RevenueLineItems' => 'Řádky obchodu',
);