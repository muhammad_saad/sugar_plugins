<?php
 // created: 2017-04-17 05:07:23

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Λογαριασμός',
  'Contacts' => 'Επαφή',
  'Tasks' => 'Εργασία',
  'Opportunities' => 'Ευκαιρία',
  'Products' => 'Γραμμή Εισηγμένων Ειδών',
  'Quotes' => 'Προσφορά',
  'Bugs' => 'Σφάλματα',
  'Cases' => 'Υπόθεση',
  'Leads' => 'Δυνητικός Πελάτης',
  'Project' => 'Έργο',
  'ProjectTask' => 'Εργασία Έργου',
  'Prospects' => 'Στόχος',
  'KBContents' => 'Βάση Γνώσεων',
  'RevenueLineItems' => 'Γραμμή Εσόδων',
);