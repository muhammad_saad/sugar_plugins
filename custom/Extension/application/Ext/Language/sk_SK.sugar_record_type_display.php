<?php
 // created: 2017-04-17 05:08:03

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Účet',
  'Opportunities' => 'Obchodná príležitosť',
  'Cases' => 'Prípad',
  'Leads' => 'Príležitosť',
  'Contacts' => 'Kontakty',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Ponuka',
  'Bugs' => 'Chyba',
  'Project' => 'Projekt',
  'Prospects' => 'Cieľ',
  'ProjectTask' => 'Projektová úloha',
  'Tasks' => 'Úloha',
  'KBContents' => 'Báza znalostí',
  'RevenueLineItems' => 'Položky krivky výnosu',
);