<?php
 // created: 2017-04-17 05:07:22

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Firma',
  'Opportunities' => 'Verkaufschance',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Contacts' => 'Kontakte',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Project' => 'Projekt',
  'Prospects' => 'Zielkontakt',
  'ProjectTask' => 'Projektaufgabe',
  'Tasks' => 'Aufgabe',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);