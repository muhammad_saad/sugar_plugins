<?php
 // created: 2017-04-17 05:07:26

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cuenta',
  'Opportunities' => 'Oportunidad',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Contacts' => 'Contactos',
  'Products' => 'Línea de la Oferta',
  'Quotes' => 'Presupuesto',
  'Bugs' => 'Incidencia',
  'Project' => 'Proyecto',
  'Prospects' => 'Público Objetivo',
  'ProjectTask' => 'Tarea de Proyecto',
  'Tasks' => 'Tarea',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Líneas de Ingreso',
);