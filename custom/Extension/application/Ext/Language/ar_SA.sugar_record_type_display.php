<?php
 // created: 2017-04-17 05:08:10

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'الحساب',
  'Opportunities' => 'الفرصة',
  'Cases' => 'الحالة',
  'Leads' => 'العميل المتوقع',
  'Contacts' => 'جهات الاتصال',
  'Products' => 'البند المسعر',
  'Quotes' => 'عرض السعر',
  'Bugs' => 'الخطأ',
  'Project' => 'المشروع',
  'Prospects' => 'الهدف',
  'ProjectTask' => 'مهمة المشروع',
  'Tasks' => 'المهمة',
  'KBContents' => 'قاعدة المعارف',
  'RevenueLineItems' => 'بنود العائدات',
);