<?php
 // created: 2017-04-17 05:07:50

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Uppgift',
  'Opportunities' => 'Affärsmöjlighet',
  'Products' => 'Produkt',
  'Quotes' => 'Offert',
  'Bugs' => 'Buggar',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektuppgift',
  'Prospects' => 'Mål',
  'KBContents' => 'Kunskapsbas Dokument',
  'RevenueLineItems' => 'Intäktsposter',
);