<?php

$mod_strings['LBL_MAILCHIMP_LIST_FIELDS'] = 'MailChimp List Field';
$mod_strings['LBL_MAILCHIMP_MODULE'] = 'New Subscriber Created as';
$mod_strings['LBL_SUGAR_TO_MC'] = 'Sync Sugar to MailChimp';
$mod_strings['LBL_MC_TO_SUGAR'] = 'Sync MailChimp to Sugar';
$mod_strings['LBL_MAILCHIMP_LIST_ID'] = 'MailChimp List Id';
$mod_strings['LBL_MC_FIELD_MAPPING'] = 'Field Mapping';
$mod_strings['LBL_IS_CONFIG_SYNCED'] = 'Is Config Synced?';
$mod_strings['LBL_IS_SYNCED'] = 'Is Synced?';
$mod_strings['LBL_LAST_SYNC_DATE'] = 'Last Sync Date';
