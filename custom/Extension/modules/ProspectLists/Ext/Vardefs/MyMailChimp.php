<?php

$dictionary['ProspectList']['fields']['mailchimp_list_id'] = array(
    'name' => 'mailchimp_list_id',
    'label' => 'LBL_MAILCHIMP_LIST_ID',
    'type' => 'varchar',
    'len' => 255,
);

$dictionary['ProspectList']['fields']['mailchimp_list_module'] = array(
    'name' => 'mailchimp_list_module',
    'label' => 'LBL_MAILCHIMP_MODULE',
    'type' => 'enum',
    'options' => 'mailchimp_list_module_dom',
    'len' => 255,
);

$dictionary['ProspectList']['fields']['mc_field_mapping'] = array(
    'name' => 'mc_field_mapping',
    'label' => 'LBL_MC_FIELD_MAPPING',
    'type' => 'text',
    'studio' => false,
);

$dictionary['ProspectList']['fields']['mc_field_details'] = array(
    'name' => 'mc_field_details',
    'label' => 'LBL_MC_FIELD_DETAILS',
    'type' => 'text',
    'studio' => false,
);

$dictionary['ProspectList']['fields']['is_config_synced'] = array(
    'name' => 'is_config_synced',
    'label' => 'LBL_IS_CONFIG_SYNCED',
    'type' => 'bool',
    'studio' => false,
);

$dictionary['ProspectList']['fields']['is_synced'] = array(
    'name' => 'is_synced',
    'label' => 'LBL_IS_SYNCED',
    'type' => 'bool',
);

$dictionary['ProspectList']['fields']['sugar_to_mc'] = array(
    'name' => 'sugar_to_mc',
    'label' => 'LBL_SUGAR_TO_MC',
    'type' => 'bool',
	'default' => '1'
);

$dictionary['ProspectList']['fields']['mc_to_sugar'] = array(
    'name' => 'mc_to_sugar',
    'label' => 'LBL_MC_TO_SUGAR',
    'type' => 'bool',
);

$dictionary['ProspectList']['fields']['last_sync_date'] = array(
    'name' => 'last_sync_date',
    'label' => 'LBL_LAST_SYNC_DATE',
    'type' => 'datetimecombo',
);
