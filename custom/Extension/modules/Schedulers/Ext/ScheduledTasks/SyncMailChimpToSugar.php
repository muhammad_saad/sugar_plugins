<?php

array_push($job_strings, 'SyncMailChimpToSugar');

function SyncMailChimpToSugar() {
	$GLOBALS['log']->fatal(" ** SyncMailChimpToSugar Job Fired ** ");
	require_once 'custom/include/MailChimp/MailChimpToSugar.php';
	$mctosugar = new MailChimpToSugar();
	if ($mctosugar->validate()) {
		$GLOBALS['log']->fatal(" ** Validated ** ");
//		$mctosugar->syncListMembers();
//		syncCampaigns($mailchimp);
	} else {
		$GLOBALS['log']->fatal(" ** Validation Failed ** ");
	}
	
	return true;
}
