<?php

array_push($job_strings, 'SyncSugarToMailChimp');

function SyncSugarToMailChimp() {
	$GLOBALS['log']->fatal(" ** SyncSugarToMailChimp Job Fired ** ");
	require_once 'custom/include/MailChimp/SugarToMailChimp.php';
	$sugartomc = new SugarToMailChimp();
	if ($sugartomc->validate()) {
		$GLOBALS['log']->fatal(" ** Validated ** ");
		$sugartomc->createList();
		$sugartomc->updateList();
		$sugartomc->syncMergeField();
		$sugartomc->createListsMembers();
		$sugartomc->updateListsMembers();
	} else {
		$GLOBALS['log']->fatal(" ** Validation Failed ** ");
	}

	return true;
}
