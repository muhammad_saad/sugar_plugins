({
	className: 'field-mapping',
	count: 0,
	events: {
		'click .addRow': 'addRow',
		'click .removeRow': 'removeRow',
		'change .dd': 'updateData',
		'change .mc_field': 'updateField',
	},
	metas: {},
	initialize: function (opts) {
		this.modules = ['Accounts', 'Contacts', 'Leads', 'Prospects'];
		this.mappings = {
			modules: this.modules,
			mapping: [{tag: 'FNAME', fields: {Accounts: '', Contacts: '', Leads: '', Prospects: ''}}, {tag: 'LNAME', fields: {Accounts: '', Contacts: '', Leads: '', Prospects: ''}}]
		};
		this.fieldDetails = {"FNAME":{"tag":"FNAME","name":"First Name","status":"Active","merge_id":1},"LNAME":{"tag":"LNAME","name":"Last Name","status":"Active","merge_id":2}};
		this._super('initialize', [opts]);
		this.model.on("sync", this.getMetas, this);
	},
	/**
	 * Event handler to add a new address field.
	 * @param {Event} evt
	 */
	addRow: function (evt) {
		if (!evt)
			return;
		value = JSON.parse(this.model.get(this.name));
		if (_.isArray(value.mapping) === false)
			value.mapping = [];
		value.mapping.push({tag: '', fields: {Accounts: '',Contacts: '',Leads: '',Prospects: ''}});
		this.model.set(this.name, JSON.stringify(value));
		this.model.set('is_config_synced', false);
		this._render();

	},
	removeRow: function (evt) {
		if (!evt)
			return;
		var $deleteButtons = this.$('.removeRow'),
				$deleteButton = this.$(evt.currentTarget),
				index = $deleteButtons.index($deleteButton),
				$removeThisField;

		$removeThisField = $deleteButton.closest('.mc-field-row');
		$removeThisField.remove();
		value = JSON.parse(this.model.get(this.name));
		var fieldDetails = JSON.parse(this.model.get(this.def.related_fields[0]));
		var mc_tag = evt.currentTarget.getAttribute('data-fieldname');
		value.mapping.splice(index, 1);
		if(!_.isEmpty(mc_tag)) {
			fieldDetails[mc_tag]['status'] = 'Deleted';
		}
		// setting values
		this.model.set('is_config_synced', false);
		this.model.set(this.name, JSON.stringify(value));
		this.model.set(this.def.related_fields[0], JSON.stringify(fieldDetails));
		this._render();
	},
	updateData: function (evt) {
		if (!evt)
			return;

		var index = this.$(evt.currentTarget).attr("index");
		var module = this.$(evt.currentTarget).attr("module");
		value = JSON.parse(this.model.get(this.name));
		value.mapping[index].fields[module] = this.$(evt.currentTarget).val();
		this.model.set('is_config_synced', false);
		this.model.set(this.name, JSON.stringify(value));
		this._render();
	},
	updateField: function (evt) {
		var index = this.$(evt.currentTarget).attr("index");
		value = JSON.parse(this.model.get(this.name));
		var fieldDetails = JSON.parse(this.model.get(this.def.related_fields[0]));
		var mc_field = this.$(evt.currentTarget).val();
		var mc_tag = evt.currentTarget.getAttribute('data-fieldname');
		// creating new tag
		if(_.isEmpty(mc_tag)) {
			mc_tag = mc_field.replace(/[^a-zA-Z]/g, "").toUpperCase();
		}
		value.mapping[index]['tag'] = mc_tag;
		if(_.isUndefined(fieldDetails[mc_tag])) {
			fieldDetails[mc_tag] = {tag: mc_tag, name: mc_field, status: 'Active', merge_id: ''};
		} else {
			fieldDetails[tag]['name'] = mc_field;
		}
		this.model.set('is_config_synced', false);
		this.model.set(this.name, JSON.stringify(value));
		this.model.set(this.def.related_fields[0], JSON.stringify(fieldDetails));
		this._render();
	},
	getMetas: function () {
		var supported_type = ['enum', 'varchar', 'date', 'datetime', 'datetimecombo', 'bool', 'text', 'phone', 'int', 'url', 'name'];
		var obj = this;
		this.metas['Accounts'] = [{name: '', label: '(Select a value)'}];
		this.metas['Contacts'] = [{name: '', label: '(Select a value)'}];
		this.metas['Leads'] = [{name: '', label: '(Select a value)'}];
		this.metas['Prospects'] = [{name: '', label: '(Select a value)'}];
		var m = App.metadata.getModules();
		_.each(m, function (value, key) {
			if (_.isArray(obj.metas[key]) && !_.isUndefined(obj.metas[key])) {
				_.each(value.fields, function (v2, k2) {
					if (_.isObject(v2)) {
						if ((_.isUndefined(v2.source) || (!_.isUndefined(v2.source) && v2.source != 'non-db'))
								&& (!_.isUndefined(v2.type)
										&& _.indexOf(supported_type, v2.type) >= 0)) {
							obj.metas[key].push({
								name: v2.name,
								vname: v2.vname,
								label: App.lang.getModString(v2.vname, key)
							});
						}
					}
				});
			}
		});
	},
	format: function (value) {
		if (_.isNull(value)) {
			value = this.mappings;
			this.model.set(this.name, JSON.stringify(value))
			return this.mappings;
		}
		var val = JSON.parse(value);
		var self = this;
		if (this.tplName == 'detail') {
			this.mappings = this.prepareDetailView(val, self);
		}
		return val;
	},
	_render: function () {
		var html = '';
		this._super("_render");
		if (this.tplName === 'edit') {
			if(_.isEmpty(this.value)) {
				this.value = this.mappings;
			}
			_.each(this.value.mapping, function (row, key) {
				html += this._buildFieldHtml(row, key);
			}, this);
			//this.$el.append(html);
			this.$el.find("#mc-field-grid").html(html);
		}
	},
	_buildFieldHtml: function (row, key) {
		if (_.isUndefined(this.metas.length)) {
			this.getMetas();
		}
		var editFieldTemplate = app.template.getField('mc-field-mapping', 'edit-row'),
				index = key;
		metas = App.utils.deepCopy(this.metas);
		return editFieldTemplate({
			index: index,
			mc_field: row.tag,
			mc_label: this.getMCFieldLabel(row.tag),
			m1_field: row.fields.Accounts,
			m2_field: row.fields.Contacts,
			m3_field: row.fields.Leads,
			m4_field: row.fields.Prospects,
			metas: metas
		});
	},
	prepareDetailView: function (val, self) {
		var mappings = {
			modules: val.modules,
			mapping: []
		};
		_.each(val.mapping, function (row) {
			var tagLabel = self.getMCFieldLabel(row.tag);
			var fields = {};
			_.each(row.fields, function (field, mod) {
				if (self.modules.indexOf(mod) <= -1) {
					return true;
				}
				var field_label = self.getFieldLabel(field, mod);
				fields[mod] = _.isEmpty(field) ? 'No Mapping' : field_label;
			});
			mappings.mapping.push({tag: tagLabel, fields: fields});
		});
		return mappings;
	},
	getFieldLabel: function(fieldName, module) {
		if (_.isEmpty(fieldName)) {
			return '';
		}
		var fields = app.metadata.getModule(module,'fields');
		var vname = (!_.isUndefined(fields[fieldName]['label'])) ? fields[fieldName]['label'] : fields[fieldName]['vname'] ;
		return app.lang.getModString(vname, module);
	},
	getMCFieldLabel: function(tag) {
		if (_.isEmpty(tag)) {
			return '';
		}
		var fieldDetails = !_.isUndefined(this.model.get(this.def.related_fields[0])) ? JSON.parse(this.model.get(this.def.related_fields[0])) : this.model.get(this.def.related_fields[0]);
		if(_.isUndefined(fieldDetails)) {
			fieldDetails = this.fieldDetails;
			this.model.set(this.def.related_fields[0], JSON.stringify(fieldDetails));
		}
		if(!_.isUndefined(fieldDetails[tag])) {
			return fieldDetails[tag]['name'];
		}
		return tag;
	}
})