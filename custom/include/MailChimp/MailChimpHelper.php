<?php

/**
 * @class MailChimpHelper
 */
class MailChimpHelper {

	public function prepareSubscriber($param) {
		$data = array();
		$data['email_address'] = "{$param['EMAIL']}";
		$data['status'] = ($param['STATUS']) ? $param['STATUS'] : "subscribed";
		$data['merge_fields'] = array();
		if (count($param['merge_fields']) > 0 ) {
			foreach($param['merge_fields'] as $tag => $val) {
				$data['merge_fields'][$tag] = $val;
			}
		}
		return $data;
	}

	public function prepareMergeField($field_name, $param = array()) {
		// {"name":"FAVORITEJOKE", "type":"text"}
		$data = array();
		$data['tag'] = $param['tag'];
		$data['name'] = "{$field_name}";
		$data['type'] = ($param['type']) ? $param['type'] : "text";
		return $data;
	}

	public function prepareList($list_name, $contact_info, $param = array()) {
		global $current_user;
		$data = array();
		$data['name'] = "{$list_name}";
		$data['contact'] = $this->prepareContactInfo($contact_info);
		$data['permission_reminder'] = "You are receiving this email because you signed up for updates about {$list_name}";
		$data['campaign_defaults'] = array(
			"from_name" => isset($param['from_name']) ? "{$param['from_name']}" : "{$current_user->name}",
			"from_email" => isset($param['from_email']) ? "{$param['from_email']}" : "{$current_user->email1}",
			"subject" => "",
			"language" => "en",
		);
		$data['email_type_option'] = true;
		return $data;
	}

	public function prepareContactInfo($contact_info) {
		$contactData = array(
			'company' => $contact_info->company,
			'address1' => $contact_info->addr1,
			'address2' => $contact_info->addr2,
			'city' => $contact_info->city,
			'state' => $contact_info->state,
			'zip' => $contact_info->zip,
			'country' => $contact_info->country,
			'phone' => '',
		);
		return $contactData;
	}

}
