<?php

require_once 'custom/include/MailChimp/MyMailChimp.php';

/**
 * Description of SugarToMailChimp
 *
 * @author Saad
 */
class SugarToMailChimp {

	private $mailchimp;

	function validate() {
		global $sugar_config;
		$apiKey = $sugar_config['mailchimp_api_key'];
		$this->mailchimp = new MyMailchimp($apiKey);
		return $this->mailchimp->validate();
	}

	function createList() {
		global $sugar_config;
		require_once('include/SugarQuery/SugarQuery.php');
		$sugarQuery = new SugarQuery();
		$prospectlist = BeanFactory::newBean('ProspectLists');
		$sugarQuery->select(array('id', 'name', 'mailchimp_list_id'));
		$sugarQuery->from($prospectlist);
		$sugarQuery->where()->equals("list_type", "default");
		$sugarQuery->where()->isEmpty("mailchimp_list_id");
		$sugarQuery->where()->equals("sugar_to_mc", true);
		//set the limit 
		$sugarQuery->limit($sugar_config['mailchimp_record_sync_limit']);
		//get the compiled SQL query
		$query = $sugarQuery->compileSql();
		$GLOBALS['log']->fatal("syncList Query: {$query}");
		//fetch the result
		$result = $sugarQuery->execute();
		if (count($result) > 0) {
			foreach ($result as $list) {
				$prospectlist->retrieve($list['id']);
				$isUpdated = false;
				if (empty($list['mailchimp_list_id'])) {
					$res = $this->mailchimp->createList($list['name']);
					if (isset($res->id) && !empty($res->id)) {
						$prospectlist->is_synced = true;
						$prospectlist->mailchimp_list_id = $res->id;
						$isUpdated = true;
					}
				}
				if ($isUpdated) {
					$prospectlist->save();
				}
			}
		}
	}

	function updateList() {
		global $sugar_config;
		require_once('include/SugarQuery/SugarQuery.php');
		$sugarQuery = new SugarQuery();
		$prospectlist = BeanFactory::newBean('ProspectLists');
		$sugarQuery->select(array('id', 'name', 'mailchimp_list_id'));
		$sugarQuery->from($prospectlist);
		$sugarQuery->where()->equals("list_type", "default");
		$sugarQuery->where()->isNotEmpty("mailchimp_list_id");
		$sugarQuery->where()->equals("sugar_to_mc", true);
		//set the limit 
		$sugarQuery->limit($sugar_config['mailchimp_record_sync_limit']);
		//get the compiled SQL query
		$query = $sugarQuery->compileSql();
		$GLOBALS['log']->fatal("syncList Query: {$query}");
		//fetch the result
		$result = $sugarQuery->execute();
		if (count($result) > 0) {
			foreach ($result as $list) {
				$prospectlist->retrieve($list['id']);
				$isUpdated = false;
				if (!empty($list['mailchimp_list_id'])) {
					$res = $this->mailchimp->updateList($list['mailchimp_list_id'], $list['name']);
					if (isset($res->id) && !empty($res->id)) {
						$prospectlist->is_synced = true;
						$isUpdated = true;
					} else if ($res->status == 404) {
						$res = $this->mailchimp->createList($list['name']);
						if (isset($res->id) && !empty($res->id)) {
							$prospectlist->is_synced = true;
							$prospectlist->mailchimp_list_id = $res->id;
							$isUpdated = true;
						}
					}
				}
				if ($isUpdated) {
					$prospectlist->save();
				}
			}
		}
	}

	function syncMergeField() {
		$defaultFields = array(
			'EMAIL',
			'FNAME',
			'LNAME',
		);
		global $sugar_config;
		require_once('include/SugarQuery/SugarQuery.php');
		$sugarQuery = new SugarQuery();
		$prospectlist = BeanFactory::newBean('ProspectLists');
		$sugarQuery->select(array('id', 'name', 'mailchimp_list_id', 'mc_field_details'));
		$sugarQuery->from($prospectlist);
		$sugarQuery->where()->equals("sugar_to_mc", true);
		$sugarQuery->where()->equals("list_type", "default");
		$sugarQuery->where()->isNotEmpty("mailchimp_list_id");
		$sugarQuery->where()->equals("is_config_synced", false);
		//set the limit 
		$sugarQuery->limit($sugar_config['mailchimp_record_sync_limit']);
		$query = $sugarQuery->compileSql();
		$GLOBALS['log']->fatal("syncMergeField Query: {$query}");
		//fetch the result
		$result = $sugarQuery->execute();
		if (count($result) > 0) {
			foreach ($result as $list) {
				if (empty($list['mc_field_details'])) {
					continue;
				}
				$isUpdated = false;
				$count = 0;
				$fieldMapping = json_decode($list['mc_field_details']);
				$prospectlist->retrieve($list['id']);
				foreach ($fieldMapping as $key => $field) {
					$count++;
					if ($field->status == 'Active' && !in_array($field->tag, $defaultFields)) {
						if (empty($field->merge_id)) {
							$res = $this->mailchimp->createMergeField($list['mailchimp_list_id'], $field->name, array('tag' => $field->tag));
						} else {
							$res = $this->mailchimp->updateMergeField($list['mailchimp_list_id'], $field->merge_id, array('name' => $field->name));
						}
						if ($res->merge_id) {
							$field->merge_id = $res->merge_id;
							$fieldMapping->$key = $field;
							$isUpdated = true;
						} else if ($res->status && $res->status == '400') {
							$field->merge_id = $count;
							$fieldMapping->$key = $field;
							$isUpdated = true;
						}
					} else if ($field->status == 'Deleted') {
						if (!empty($field->merge_id)) {
							$res = $this->mailchimp->deleteMergeField($list['mailchimp_list_id'], $field->merge_id);
						} else {
							unset($fieldMapping->$key);
						}
						$isUpdated = true;
					} else {
						
					}
				}
				if ($isUpdated) {
					$prospectlist->mc_field_details = json_encode($fieldMapping);
					$prospectlist->is_config_synced = true;
					$prospectlist->save();
				}
			}
		}
	}

	function createListsMembers() {
		global $sugar_config;
		require_once('include/SugarQuery/SugarQuery.php');
		$sugarQuery = new SugarQuery();
		$mcconfig = BeanFactory::newBean('z_MCConfig');
		$sugarQuery->from($mcconfig);
		//adding join
		$sugarQuery->joinRaw("INNER JOIN prospect_lists ON z_mcconfig.prospect_list_id = prospect_lists.id");

		$sugarQuery->select(array(
			'id',
			'prospect_list_id',
			'mc_list_id',
			'target_module_id',
			'target_module',
		));
		$sugarQuery->select->fieldRaw("prospect_lists.mc_field_mapping AS 'MCFieldMapping'", ''); //array("prospect_lists_prospects.related_type", 'relatedType'));
		$sugarQuery->where()->equals("sync_status", "Active");
		$sugarQuery->where()->equals("is_synced", false);
		$sugarQuery->where()->isEmpty("mc_member_id");
		$sugarQuery->where()->isNotEmpty("prospect_list_id")->isNotEmpty("mc_list_id")->isNotEmpty("target_module_id")->isNotEmpty("target_module");
		//set the limit 
		$sugarQuery->limit($sugar_config['mailchimp_record_sync_limit']);
		$query = $sugarQuery->compileSql();
		$GLOBALS['log']->fatal("CreateListMember Query: {$query}");
		//fetch the result
		$result = $sugarQuery->execute();
		if (count($result) > 0) {
			foreach ($result as $list) {
				if (empty($list['MCFieldMapping'])) {
					continue;
				}
				$mcconfig->retrieve($list['id']);
				$mergeFields = array();
				$fieldMapping = json_decode($list['MCFieldMapping']);
				if (!empty($list['target_module_id']) && !empty($list['target_module'])) {
					$relatedId = $list['target_module_id'];
					$relatedType = $list['target_module'];
					$relatedBean = BeanFactory::getBean($relatedType);
					$relatedBean->retrieve($relatedId);
					$mergeFields['EMAIL'] = $relatedBean->email1;
					if (empty($relatedBean->email1)) {
						continue;
					}
					foreach ($fieldMapping->mapping as $row) {
						$field = $row->fields->$relatedType;
						if (!empty($field)) {
							$mergeFields['merge_fields'][$row->tag] = $relatedBean->$field;
						} else {
							$mergeFields['merge_fields'][$row->tag] = "";
						}
					}
					$res = $this->mailchimp->createListMembers($list['mc_list_id'], $mergeFields);
					if (isset($res->id) && !empty($res->id)) {
						$mcconfig->mc_member_id = $res->id;
						$mcconfig->is_synced = true;
						$mcconfig->save();
					} else if ($res->status && $res->status == 400) {
						$res = $this->mailchimp->updateListMembers($list['mc_list_id'], '', $mergeFields);
						if (isset($res->id) && !empty($res->id)) {
							$mcconfig->mc_member_id = $res->id;
							$mcconfig->is_synced = true;
							$mcconfig->save();
						}
					}
				}
			}
		}
	}

	function updateListsMembers() {
		global $sugar_config;
		require_once('include/SugarQuery/SugarQuery.php');
		$sugarQuery = new SugarQuery();
		$mcconfig = BeanFactory::newBean('z_MCConfig');
		$sugarQuery->from($mcconfig);
		//adding join
		$sugarQuery->joinRaw("INNER JOIN prospect_lists ON z_mcconfig.prospect_list_id = prospect_lists.id");

		$sugarQuery->select(array(
			'id',
			'sync_status',
			'prospect_list_id',
			'mc_list_id',
			'mc_member_id',
			'target_module_id',
			'target_module',
		));
		$sugarQuery->select->fieldRaw("prospect_lists.mc_field_mapping AS 'MCFieldMapping'", '');
		$sugarQuery->where()->equals("is_synced", false);
		$sugarQuery->where()->isNotEmpty("mc_member_id")->isNotEmpty("prospect_list_id")->isNotEmpty("mc_list_id")->isNotEmpty("target_module_id")->isNotEmpty("target_module");
		//set the limit 
		$sugarQuery->limit($sugar_config['mailchimp_record_sync_limit']);
		//get the compiled SQL query
		$query = $sugarQuery->compileSql();
		$GLOBALS['log']->fatal("syncList Query: {$query}");
		//fetch the result
		$result = $sugarQuery->execute();
		if (count($result) > 0) {
			foreach ($result as $list) {
				if (empty($list['MCFieldMapping'])) {
					continue;
				}
				$mcconfig->retrieve($list['id']);
				$mergeFields = array();
				$fieldMapping = json_decode($list['MCFieldMapping']);
				if (!empty($list['target_module_id']) && !empty($list['target_module'])) {
					$relatedId = $list['target_module_id'];
					$relatedType = $list['target_module'];
					$relatedBean = BeanFactory::getBean($relatedType);
					$relatedBean->retrieve($relatedId);
					$mergeFields['EMAIL'] = $relatedBean->email1;
					foreach ($fieldMapping->mapping as $row) {
						$field = $row->fields->$relatedType;
						if (!empty($field)) {
							$mergeFields['merge_fields'][$row->tag] = $relatedBean->$field;
						} else {
							$mergeFields['merge_fields'][$row->tag] = "";
						}
					}
					if ($list['sync_status'] == 'Active') {
						$res = $this->mailchimp->updateListMembers($list['mc_list_id'], $list['mc_member_id'], $mergeFields);
					} else if ($list['sync_status'] == 'Deleted') {
						$res = $this->mailchimp->deleteListMembers($list['mc_list_id'], $list['mc_member_id']);
						$mcconfig->mc_member_id = '';
					}
					if (isset($res->id) || empty($res)) {
						$mcconfig->is_synced = true;
						$mcconfig->save();
					}
				}
			}
		}
	}

}
