<?php

require_once 'custom/include/MailChimp/MyMailChimp.php';

/**
 * Description of MailChimpToSugar
 *
 * @author Saad
 */
class MailChimpToSugar {

	private $mailchimp;

	function validate() {
		global $sugar_config;
		$apiKey = $sugar_config['mailchimp_api_key'];
		$this->mailchimp = new MyMailchimp($apiKey);
		return $this->mailchimp->validate();
	}

	function syncListMembers() {
		$lists = $this->mailchimp->getLists();
//	$GLOBALS['log']->fatal(print_r($lists, 1));
		if (count($lists->lists) > 0) {
			foreach ($lists->lists as $list) {
//			$GLOBALS['log']->fatal(print_r($list, 1));
				$listId = $list->id;
				$members = $this->mailchimp->getListMembers($listId);
//				$GLOBALS['log']->fatal(print_r($members, 1));
				$this->syncMembers($listId, $members);
			}
		}
	}

	function syncCampaigns($mailchimp) {
		$campaigns = $this->mailchimp->getCampagins();
		if (count($campaigns->campaigns) > 0) {
			foreach ($campaigns->campaigns as $camp) {
				$campaign = BeanFactory::getBean("Campaigns");
				$campaign->retrieve_by_string_fields(array('mc_id' => $camp->id));
				if (!empty($campaign->id)) {
					$this->updateReportSummary($campaign);
				}
			}
		}
	}

	function updateReportSummary($campaign) {
		$campaign->name = '';
	}

	/**
	 * 
	 * @param type $listId
	 * @param type $members
	 */
	function syncMembers($listId, $members) {
		$prospectlist = BeanFactory::getBean("ProspectLists");
		$prospectlist->retrieve_by_string_fields(array('mailchimp_list' => $listId));
		if (count($members->members) > 0 && !empty($prospectlist->id)) {
			$this->syncSubscriber($prospectlist, $members);
		}
	}

	/**
	 * 
	 * @param type $list_id
	 */
	function syncSubscriber($prospectlist, $members) {
		$GLOBALS['log']->fatal(" SYNC SUBSCRIBERS ");
		$subscriber_module = $prospectlist->mailchimp_module_list;
		$link_name = strtolower($subscriber_module);
		$prospectlist->load_relationship($link_name);
		foreach ($members->members as $member) {
			$GLOBALS['log']->fatal(print_r($member, 1));
			$subscriber = BeanFactory::newBean($subscriber_module);
			$subscriber->retrieve_by_string_fields(array('mailchimp_member_id' => $member->id));
			$subscriberId = $subscriber->id;
			$subscriber->mailchimp_member_id = $member->id;
			$subscriber->first_name = $member->merge_fields->FNAME;
			$subscriber->last_name = $member->merge_fields->LNAME;
			$subscriber->mailchimp_rating_c = $member->member_rating;
			$subscriber->save();
			if (empty($subscriberId)) {
				// Adding Email Address
				$email = new SugarEmailAddress();
				$email->addAddress($member->email_address, true);
				$email->save($subscriber->id, $subscriber_module);
				// Linking with Subscriber module
				if ($member->status == "subscribed") {
					$prospectlist->$link_name->add($subscriber->id);
				}
			} else {
				if ($member->status != "subscribed") {
					$prospectlist->$link_name->delete($prospectlist->id, $subscriber->id);
				} else {
					$prospectlist->$link_name->add($subscriber->id);
				}
			}
		}
	}

}
