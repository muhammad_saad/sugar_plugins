<?php

require_once 'custom/include/MailChimp/MailChimpHelper.php';

/**
 * @class Mailchimp
 */
class MyMailChimp {

	public $data_center;
	public $api_key;
	public $url;
	private $helper;
	private $contact_info;

	function MyMailChimp($key) {
		$this->setApiKey($key);
		$this->helper = new MailChimpHelper();
		$this->data_center = substr($this->api_key, strpos($this->api_key, '-') + 1);
		$this->url = "https://{$this->data_center}.api.mailchimp.com/3.0/";
	}

	function setApiKey($key) {
		global $sugar_config;
		$this->api_key = empty($key) ? $sugar_config['mailchimp_api_key'] : $key;
	}

	function getApiKey() {
		return $this->api_key;
	}

	function callAPI($url, $apiKey, $method = "GET", $postData = array()) {
		$postStr = false;
		if (count($postData) > 0) {
			$postStr = json_encode($postData);
		}
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if (in_array($method, array('POST', 'PUT', 'PATCH'))) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr);
		}
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result);
	}

	public function validate() {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - validate] *** ");
		$response = $this->callAPI($this->url, $this->api_key, 'GET');
		$GLOBALS['log']->debug(print_r($response, 1));
		if (isset($response->account_id) && !empty($response->account_id)) {
			$this->contact_info = $response->contact;
			return true;
		} else {
			return false;
		}
	}

	public function getLists() {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - getLists] *** ");
		$response = $this->callAPI($this->url . 'lists', $this->api_key, 'GET');
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function getList($list_id) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - getList: $list_id] *** ");
		$response = $this->callAPI($this->url . "lists/{$list_id}", $this->api_key, 'GET');
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}
	
	public function createList($list_name, $param=array()) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - createList: $list_name] *** ");
		$data = $this->helper->prepareList($list_name, $this->contact_info, $param);
		$response = $this->callAPI($this->url . "lists", $this->api_key, 'POST', $data);
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function updateList($list_id, $list_name, $param=array()) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - updateList: $list_id] *** ");
		$data = $this->helper->prepareList($list_name, $this->contact_info, $param);
		$response = $this->callAPI($this->url . "lists/{$list_id}", $this->api_key, 'PATCH', $data);
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function createMergeField($list_id, $field_name, $param=array()) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - createMergeField: $list_id - ($field_name)] *** ");
		$data = $this->helper->prepareMergeField($field_name, $param);
		$response = $this->callAPI($this->url . "lists/{$list_id}/merge-fields", $this->api_key, 'POST', $data);
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}
	
	public function updateMergeField($list_id, $field_id, $param=array()) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - createMergeField: $list_id - ($field_id)] *** ");
		$data = $param;
		$response = $this->callAPI($this->url . "lists/{$list_id}/merge-fields/{$field_id}", $this->api_key, 'PATCH', $data);
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}
	
	public function deleteMergeField($list_id, $field_id) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - createMergeField: $list_id - ($field_id)] *** ");
		$response = $this->callAPI($this->url . "lists/{$list_id}/merge-fields/{$field_id}", $this->api_key, 'DELETE', array());
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function getListMembers($list_id) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - getListMembers: $list_id] *** ");
		$response = $this->callAPI($this->url . "lists/{$list_id}/members", $this->api_key, 'GET');
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function createListMembers($list_id, $data) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - createListMembers: $list_id - {$data['EMAIL']}] *** ");
		$merge_fields = $this->helper->prepareSubscriber($data);
		$response = $this->callAPI($this->url . "lists/{$list_id}/members", $this->api_key, 'POST', $merge_fields);
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function updateListMembers($list_id, $subscriber_hash, $data) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - updateListMembers: $list_id - {$data['EMAIL']}] *** ");
		$merge_fields = $this->helper->prepareSubscriber($data);
		$subscriber_hash = empty($subscriber_hash) ? md5(strtolower($data['EMAIL'])) : $subscriber_hash;
		$response = $this->callAPI($this->url . "lists/{$list_id}/members/{$subscriber_hash}", $this->api_key, 'PUT', $merge_fields);
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function deleteListMembers($list_id, $subscriber_hash) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - deleteListMembers: $list_id - {$subscriber_hash}] *** ");
		$response = $this->callAPI($this->url . "lists/{$list_id}/members/{$subscriber_hash}", $this->api_key, 'DELETE');
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function getListMember($list_id, $subscriber_hash) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - getListMember: $list_id ($subscriber_hash)] *** ");
		$response = $this->callAPI($this->url . "lists/{$list_id}/members/{$subscriber_hash}", $this->api_key, 'GET');
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function getCampaigns() {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - getCampaigns] *** ");
		$response = $this->callAPI($this->url . 'campaigns', $this->api_key, 'GET');
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function getCampaign($campaign_id) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - getCampaign: $campaign_id] *** ");
		$response = $this->callAPI($this->url . "campaigns/{$campaign_id}", $this->api_key, 'GET');
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

	public function getCampaignContent($campaign_id) {
		$GLOBALS['log']->fatal(" *** [MAILCHIMP - getCampaignContent: $campaign_id] *** ");
		$response = $this->callAPI($this->url . "campaigns/{$campaign_id}/content", $this->api_key, 'GET');
		$GLOBALS['log']->debug(print_r($response, 1));
		return $response;
	}

}
