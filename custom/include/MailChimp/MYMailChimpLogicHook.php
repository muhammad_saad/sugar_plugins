<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/**
 * Description of MYMailChimpLogicHook
 *
 * @author Saad
 */
class MYMailChimpLogicHook {

	function AccountsAfterSave($bean, $event, $arguments) {
		$GLOBALS['log']->fatal(" ** Accounts AfterSave Fired ** ");
		$mcconfig = BeanFactory::getBean("z_MCConfig");
		$mcconfig->retrieve_by_string_fields(array('target_module_id' => $bean->id, 'email_address' => $bean->email1));
		if (!empty($mcconfig->id)) {
			$mcconfig->is_synced = false;
			$mcconfig->save();
		}
	}

	function ContactsAfterSave($bean, $event, $arguments) {
		$GLOBALS['log']->fatal(" ** Contacts AfterSave Fired ** ");
		$mcconfig = BeanFactory::getBean("z_MCConfig");
		$mcconfig->retrieve_by_string_fields(array('target_module_id' => $bean->id, 'email_address' => $bean->email1));
		if (!empty($mcconfig->id)) {
			$mcconfig->is_synced = false;
			$mcconfig->save();
		}
	}

	function LeadsAfterSave($bean, $event, $arguments) {
		$GLOBALS['log']->fatal(" ** Leads AfterSave Fired ** ");
		$mcconfig = BeanFactory::getBean("z_MCConfig");
		$mcconfig->retrieve_by_string_fields(array('target_module_id' => $bean->id, 'email_address' => $bean->email1));
		if (!empty($mcconfig->id)) {
			$mcconfig->is_synced = false;
			$mcconfig->save();
		}
	}

	function ProspectsAfterSave($bean, $event, $arguments) {
		$GLOBALS['log']->fatal(" ** Prospects AfterSave Fired ** ");
		$mcconfig = BeanFactory::getBean("z_MCConfig");
		$mcconfig->retrieve_by_string_fields(array('target_module_id' => $bean->id, 'email_address' => $bean->email1));
		if (!empty($mcconfig->id)) {
			$mcconfig->is_synced = false;
			$mcconfig->save();
		}
	}

	function ProspectListsAfterSave($bean, $event, $arguments) {
		global $db;
		$prospect_list_id = $bean->id;
		$mc_list_id = $bean->mailchimp_list_id;
		$query = "UPDATE z_mcconfig SET is_synced = 0, mc_list_id = '{$mc_list_id}' WHERE prospect_list_id = '{$prospect_list_id}' AND sync_status != 'Deleted' AND deleted = 0";
		$db->query($query);
	}

	function ProspectListsAfterRelationshipAdd($bean, $event, $arguments) {
		$GLOBALS['log']->fatal(" ** ProspectLists AfterRelationshipAdd Fired ** ");
		$GLOBALS['log']->fatal(print_r($arguments, 1));
		$prospect_list_id = $arguments['id'];
		$target_module_id = $arguments['related_id'];
		$target_module = $arguments['related_module'];
		$target = BeanFactory::getBean($target_module);
		$target->retrieve($target_module_id);
		$mcconfig = BeanFactory::getBean("z_MCConfig");
		$mcconfig->retrieve_by_string_fields(array('prospect_list_id' => $prospect_list_id, 'email_address' => $target->email1));
		$mcconfig->name = "{$arguments['name']} - {$target_module} - {$arguments['related_name']}";
		$mcconfig->target_module_id = $target_module_id;
		$mcconfig->target_module = $target_module;
		$mcconfig->mc_list_id = $bean->mailchimp_list_id;
		$mcconfig->prospect_list_id = $prospect_list_id;
		$mcconfig->is_synced = false;
		$mcconfig->sync_status = 'Active';
		$mcconfig->email_address = $target->email1;
		if (empty($mcconfig->id) && !empty($mcconfig->email_address)) {
			$mcconfig->save();
		}
	}

	function ProspectListsAfterRelationshipDelete($bean, $event, $arguments) {
		$GLOBALS['log']->fatal(" ** ProspectLists AfterRelationshipDelete Fired ** ");
		global $db;
		$GLOBALS['log']->fatal(print_r($arguments, 1));
		$prospect_list_id = $arguments['id'];
		$target_module_id = $arguments['related_id'];
		$mcconfig = BeanFactory::getBean("z_MCConfig");
		$mcconfig->retrieve_by_string_fields(array('prospect_list_id' => $prospect_list_id, 'target_module_id' => $target_module_id));
		if (!empty($mcconfig->id)) {
			$mcconfig->is_synced = false;
			$mcconfig->sync_status = 'Deleted';
			$mcconfig->save();
		}
	}

}
